﻿using AutoMapper;
using WebAPI.Models;
using WebAPI.Models.DTO;
using WebAPI.Models.DTO.Franchise;

namespace WebAPI.Profiles
{
    public class FranchiseProfile : Profile
    {

        public FranchiseProfile()
        {
            //Creates maps between two different classes to only show relevant and objective data about each model, one for each DTO
            CreateMap<Franchise, ReadFranchiseDTO>()
                .ReverseMap();
            CreateMap<Franchise, CreateFranchiseDTO>()
                .ReverseMap();
        }
    }
}

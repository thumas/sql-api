﻿using AutoMapper;
using WebAPI.Models;
using WebAPI.Models.DTO.Characters;

namespace WebAPI.Profiles
{
    public class CharactersProfile : Profile
    {
        public CharactersProfile()
        {
            //Creates maps between two different classes to only show relevant and objective data about each model, one for each DTO
            CreateMap<Characters, ReadCharactersDTO>()
                .ReverseMap();
            CreateMap<Characters, CreateCharactersDTO>()
                .ReverseMap();
            CreateMap<Characters, EditCharactersDTO>()
                .ReverseMap();
        }
    }
}

﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Models.DTO;
using WebAPI.Models.DTO.Characters;
using WebAPI.Models.DTO.Franchise;

namespace WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MoviesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Gets a list of Movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadMoviesDTO>>> GetMovies()
        {
            return _mapper.Map<List<ReadMoviesDTO>>( await _context.Movies.ToListAsync());
        }

        /// <summary>
        /// Gets a specific Movie based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadMoviesDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadMoviesDTO>(movie);
        }

        /// <summary>
        /// Gets all characters in a movie
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<List<ReadCharactersDTO>>> GetCharactersInMovie(int id)
        {
            var movie = _context.Movies.Include(p => p.Characters).FirstOrDefault(p => p.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<ReadCharactersDTO>>(movie.Characters.ToList());
        }


        /// <summary>
        /// Updates a specific Movie based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }
            Movie domainMovies = _mapper.Map<Movie>(dtoMovie);
            _context.Entry(domainMovies).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        /// <summary>
        /// Update characters in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateCharactersInMovie(int id, List<int> characters)
        {
            var movies = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
            if (movies == null)
            {
                return NotFound();
            }
            var characterInList = new List<Characters>();
            foreach (var character in characters)
            {
                var findChar = await _context.Characters.FindAsync(character);
                if (findChar != null)
                {
                    movies.Characters.Add(findChar);
                }
            }
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Removes a characters from a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> RemoveCharacterFromMovie(int movieId, int characterId)
        {

            var movie = await _context.Movies.Include(p => p.Characters).FirstOrDefaultAsync(p => p.Id == movieId);
            var characterToRemove = await _context.Characters.FirstOrDefaultAsync(q => q.Id == characterId);

            movie.Characters.Remove(characterToRemove);

            await _context.SaveChangesAsync();

            return NoContent();
        }


        /// <summary>
        /// Creates a new Movie.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(CreateMoviesDTO dtoMovie)
        {
            Movie movieDomain = _mapper.Map<Movie>(dtoMovie);
            _context.Movies.Add(movieDomain);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie"
                , new { id = movieDomain.Id },
                _mapper.Map<CreateMoviesDTO>(movieDomain));
        }

        /// <summary>
        /// Deletes a specific Movie based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
 
        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}

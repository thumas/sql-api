﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data;
using WebAPI.Models;
using WebAPI.Models.DTO;
using WebAPI.Models.DTO.Characters;
using WebAPI.Models.DTO.Franchise;

namespace WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MoviesContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MoviesContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets a list of franchise
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadFranchiseDTO>>> GetFranchises()
        {

            //Gonna fix this.
            var franchiseList = _mapper.Map<List<ReadFranchiseDTO>>(await _context.Franchises.ToArrayAsync());
            return franchiseList;
        }

        /// <summary>
        /// Gets all movies within a franchise
        /// </summary>
        ///  <remarks>Loads all Movie objects located within a Franchise, note: franchise 2 does not include any movie in our db.</remarks>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<ReadMoviesDTO>>> GetFranchisesWithMovies(int id)
        {
            var franch = _context.Franchises.Include(p => p.Movies).FirstOrDefault(p => p.Id == id);

            if (franch == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<ReadMoviesDTO>>(franch.Movies.ToList());
        }



        /// <summary>
        /// Gets all unique characters in each franchise
        /// </summary>
        /// <remarks>Loads all character objects located within a movie. We have decided to not include duplicate characters. Franchise 2 does not include any characters in our database</remarks>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<ReadCharactersDTO>>> GetFranchisesWithCharacters(int id)
        {
            var franch =  _context.Franchises.Include(p => p.Movies).ThenInclude(m => m.Characters).FirstOrDefault(p => p.Id == id);

            List<Characters> listOfChar = new List<Characters>();

           if(franch == null)
            {
                return NotFound();
            }
           foreach(Movie movie in franch.Movies)
            {
                foreach(Characters character in movie.Characters)
                {
                    listOfChar.Add(character);
                }
            }

            return _mapper.Map<List<ReadCharactersDTO>>(listOfChar.Distinct().ToList());
        }



        /// <summary>
        /// Gets a specific Franchise based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadFranchiseDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map< ReadFranchiseDTO> (franchise);
        }




        /// <summary>
        /// Updates a specific Franchise based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, ReadFranchiseDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Updates which movies should be included specific Franchise based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
       [HttpPut]
        public async Task UpdateMoviesInFranchise(int franchId, List<int> moviesList)
        {
            Franchise franchToUpdateMovies = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.Id == franchId)
                .FirstAsync();

            List<Movie> movies = new();
            foreach (int movieId in moviesList)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                    throw new KeyNotFoundException();
                movies.Add(movie);
            }
            franchToUpdateMovies.Movies = movies;
            await _context.SaveChangesAsync();
        }


        /// <summary>
        /// Creates a new Franchise.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise( CreateFranchiseDTO dtoFranchise)
        {
            Franchise franchiseDomain = _mapper.Map<Franchise>(dtoFranchise);
            _context.Franchises.Add(franchiseDomain);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise",
                new { id = franchiseDomain.Id },
                _mapper.Map<CreateFranchiseDTO>(franchiseDomain));
        }


        /// <summary>
        /// Deletes a specific Franchise based on the Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }


    }
}

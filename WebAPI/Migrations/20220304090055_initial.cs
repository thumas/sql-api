﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebAPI.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CharactersMovie",
                columns: table => new
                {
                    CharactersId = table.Column<int>(type: "int", nullable: false),
                    MoviesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharactersMovie", x => new { x.CharactersId, x.MoviesId });
                    table.ForeignKey(
                        name: "FK_CharactersMovie_Characters_CharactersId",
                        column: x => x.CharactersId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharactersMovie_Movies_MoviesId",
                        column: x => x.MoviesId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, null, "Male", "Martin Beck", "https://d2iltjk184xms5.cloudfront.net/uploads/photo/file/398271/large_96d68e3caff973a9e9a1695a287122b7-Martin-Beck.jpeg" },
                    { 2, null, "Male", "Gunvald Larsson", "https://d2iltjk184xms5.cloudfront.net/uploads/photo/file/181220/36667d2682a9ac81badb589957bad96a-gunvald.jpg" },
                    { 3, "Tormund", "Male", "Steinar Hovland", "https://images.aftonbladet-cdn.se/v2/images/64545be9-e38e-4c01-be59-45e76c2b82d2?fit=crop&format=auto&h=994&q=50&w=1900&s=40dec49afa600cbe15caa261fb10f0584d00929e" },
                    { 4, "Father Death", "Male", "Ghostface", "https://cdn.vox-cdn.com/thumbor/MfPfogfiF7COFcq4mrgyCct88n0=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/16305938/Ghost_Face.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "No franchise, used for movies that are not part of a franchise", "Uncategorized" },
                    { 2, "An American film series that comprises five parody films mainly focusing on spoofing horror films", "Scary Movie" },
                    { 3, "A series of films, started off as racing movies and ended up as mad action movies", "The Fast and Furious" },
                    { 4, "Swedish crime film series", "Beck" },
                    { 5, "American meta horror slasher", "Scream" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "Harald Hamrell", 4, "Thriller, Crime, Action", "Sista Vittnet", "https://upload.wikimedia.org/wikipedia/en/thumb/c/c1/Beck_16_-_Sista_vittnet.jpg/220px-Beck_16_-_Sista_vittnet.jpg", 2002, "https://www.youtube.com/watch?v=2VS0a9B6ww4" },
                    { 2, "Lisa Ohlin", 4, "Thriller, Crime, Action", "58 Minuter", "https://img-cdn.b17g.net/0348462b-d5b7-427c-b016-c3e93f3fbe30/crop2x3.jpg", 2022, "https://www.youtube.com/watch?v=gwZ0HZTHJSA" },
                    { 3, "Wes Craven", 5, "Horror", "Scream", "https://img-cdn.b17g.net/0348462b-d5b7-427c-b016-c3e93f3fbe30/crop2x3.jpg", 1996, "https://www.youtube.com/watch?v=AWm_mkbdpCA" },
                    { 4, "Wes Craven", 5, "Horror", "Scre4m", "https://m.media-amazon.com/images/M/MV5BMjA2NjU5MTg5OF5BMl5BanBnXkFtZTgwOTkyMzQxMDE@._V1_.jpg", 2011, "https://www.youtube.com/watch?v=JKRtyVLWV-E" },
                    { 5, "Keenen Ivory Wayans", 2, "Parody, Comedy", "Scary Movie", "https://m.media-amazon.com/images/M/MV5BMGEzZjdjMGQtZmYzZC00N2I4LThiY2QtNWY5ZmQ3M2ExZmM4XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg", 2000, "https://www.youtube.com/watch?v=_dktIVAfjzY" }
                });

            migrationBuilder.InsertData(
                table: "CharactersMovie",
                columns: new[] { "CharactersId", "MoviesId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 2, 1 },
                    { 3, 2 },
                    { 4, 3 },
                    { 4, 4 },
                    { 4, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharactersMovie_MoviesId",
                table: "CharactersMovie",
                column: "MoviesId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharactersMovie");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}

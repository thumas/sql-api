﻿namespace WebAPI.Models.DTO.Characters
{
    public class EditCharactersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string? Alias { get; set; }

        public string? Gender { get; set; }

        public string? Picture { get; set; }
    }
}

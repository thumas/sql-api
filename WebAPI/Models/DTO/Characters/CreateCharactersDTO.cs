﻿namespace WebAPI.Models.DTO.Characters
{
    public class CreateCharactersDTO
    {
        public string Name { get; set; }

        public string? Alias { get; set; }

        public string? Gender { get; set; }

        public string? Picture { get; set; }
    }
}

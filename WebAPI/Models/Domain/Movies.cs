﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Movie
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]

        public string MovieTitle { get; set; }
        [MaxLength(50)]

        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        
        public string? Picture { get; set; }
        [MaxLength(100)]

        public string? Trailer { get; set; }

        public int FranchiseId { get; set; } // --- "Foreign key" 

        public Franchise Franchise { get; set; }

        public ICollection<Characters> Characters { get; set; }
    }
}

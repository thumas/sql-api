﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    public class Characters
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]

        public string Name { get; set; }
        [MaxLength(50)]

        public string? Alias { get; set; }
        [MaxLength(50)]

        public string? Gender { get; set; }

        public string? Picture { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
